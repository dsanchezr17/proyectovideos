# -*- coding: utf-8 -*-

import scrapy
from scrapy.item import Field
from scrapy.item import Item
from scrapy.spiders import Spider
from scrapy.selector import Selector
from scrapy.loader import ItemLoader
from scrapy.loader.processors import MapCompose

class Pregunta(Item):

    apuesta = Field()
    #pregunta = Field()
    id = Field()
     

body = driver.execute_script("return document.body")
source = body.get_attribute('innerHTML') 
  

class StackOverFlowSpider(Spider):
    name = "mi primer Scraping"
    #start_urls = ['https://betplay.com.co/apuestas#filter/football']
    start_urls = ['https://www.rivalo.co/es/apuestas/futbol-colombia/gchecab/']
    
    def parse(self,response):

        sel = Selector(response)
        preguntas = sel.xpath('//*[@class="border_ccc"]/div')
    #hacer un for iterando con el numero de elementos de preguntas
        for i, elem in enumerate(preguntas):
            item = ItemLoader(Pregunta(),elem)
            item.add_xpath('apuesta','.//span/text()')
            item.add_value('id',i)
            yield item.load_item()